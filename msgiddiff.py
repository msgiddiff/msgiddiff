# -*- coding: utf-8 -*-
#
# Copyright (C) 2009-2011 Claude Paroz <claude@2xlibre.net>
#               2011 Bruno Brouard <annoa.b@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import os
import re
import subprocess
import tempfile
from gettext import gettext as _

from gi.repository import GObject, Gtk, Gedit


# Insert a new item in the Tools menu
ui_str = """<ui>
  <menubar name="MenuBar">
    <menu name="ToolsMenu" action="Tools">
      <placeholder name="ToolsOps_3">
        <menuitem name="MsgidDiff" action="MsgidDiff"/>
      </placeholder>
    </menu>
  </menubar>
</ui>
"""

class MsgiddiffPlugin(GObject.Object, Gedit.WindowActivatable):
    __gtype_name__ = "MsgidDiff"

    window = GObject.property(type=Gedit.Window)

    def __init__(self):
        GObject.Object.__init__(self)

    def _insert_menu(self):
        # Add a menu item to Gedit Tools menu
        manager = self.window.get_ui_manager()
        self._actions = Gtk.ActionGroup("MsgidDiffPluginActions")
        self._actions.add_actions([("MsgidDiff", None, _("Diff msgids"),
                                         None, _("Diff msgids with Meld"),
                                         self.on_diff_msgids_activate),])
        manager.insert_action_group(self._actions)

        # Merge the UI
        self._ui_merge_id = manager.add_ui_from_string(ui_str)
        manager.ensure_update()

    def do_activate(self):
        self._insert_menu()

    def do_deactivate(self):
        self._remove_menu()

    def _remove_menu(self):
        manager = self.window.get_ui_manager()
        manager.remove_ui(self._ui_merge_id)
        manager.remove_action_group(self._actions)
        manager.ensure_update()

    def do_update_state(self):
        pass

    def show_error(self, message):
        dialog = Gtk.MessageDialog(
            self.window, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, message)
        dialog.run()
        dialog.destroy()

    def on_diff_msgids_activate(self, action, data=None):
        view = self.window.get_active_view()
        buf = view.get_buffer()
        cursor = buf.get_insert()
        current_iter = buf.get_iter_at_mark(cursor)
        start_iter = buf.get_start_iter()
        end_iter = buf.get_end_iter()

        # Search backward for "#| msgid \"", then msgid ", then msgstr "
        search_old = current_iter.backward_search(
            "#| msgid \"", Gtk.TextSearchFlags.TEXT_ONLY, start_iter)
        if not search_old:
            self.show_error("Unable to find old msgid")
            return
        iter1 = search_old[1]
        search_msgid = iter1.forward_search(
            '"\nmsgid "', Gtk.TextSearchFlags.TEXT_ONLY, end_iter)
        if not search_msgid:
            self.show_error("Unable to find new msgid")
            return

        iter2, iter3 = search_msgid
        search_msgstr = iter2.forward_search(
            '"\nmsgstr', Gtk.TextSearchFlags.TEXT_ONLY, end_iter)
        if not search_msgstr:
            self.show_error("Unable to find msgstr following msgid")
            return
        iter4 = search_msgstr[0]
        old_text = self.clean_text(buf.get_text(iter1, iter2, False))
        new_text = self.clean_text(buf.get_text(iter3, iter4, False))
        tmp_file1 = tempfile.NamedTemporaryFile(suffix='.txt', delete=False)
        tmp_file2 = tempfile.NamedTemporaryFile(suffix='.txt', delete=False)  
        tmp_file1.write(old_text)
        tmp_file2.write(new_text)
        tmp_file1.close()
        tmp_file2.close()
        try:
            subprocess.Popen(["meld", tmp_file1.name, tmp_file2.name])
        except OSError:
            self.show_error(
                "Unable to launch the Meld program. Make sure it is installed.")

    def clean_text(self, txt):
        # Remove line breaks, initial and trailing quotes, #| prefixes
        new_txt = re.sub('"\n(#\| )?"', '', txt)
        if 'msgid_plural' in new_txt:
            new_txt = re.sub('"\n(#\| )?msgid_plural "', '\n\nmsgid_plural ', new_txt)
        return new_txt
